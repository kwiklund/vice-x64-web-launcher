<?php

/* Copyright (c) 2013 Kristian Wiklund
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the <organization>.  The name of the
 * <organization> may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

require_once("config.php");

function c64command($c) {
	 global $socket;
 	echo("Writing $c to socket<br>");
	   sleep(1);
	   echo("$s<br>");	   
	   $n=socket_write($socket, "$c\n");
	   echo("Wrote $n bytes to socket<br>");

	   // read until eof

	  do {
	     $n = socket_read($socket, 100);
	     echo($n."<br>");
	  } while($n && !($n===''));
	 
}

function c64connectsocket() {

	 global $socket;
	 global $host;
	 global $port;


if($socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) {

	   if(!socket_connect($socket, $host, $port)) {
	      echo "Socket connect fail<br>";
	      exit;
	   }

	   socket_set_nonblock($socket); // not blocking

	   } else {
	     echo "socket create fail<br>";
	     exit;
	     }
}

function c64closesocket() {
	 global $socket;

	   socket_shutdown($socket,1);
	   sleep(2);
	   socket_shutdown($socket,0);
	   sleep(2);
	   socket_close($socket);
}
?>