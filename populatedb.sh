#!/bin/bash -f

#Copyright (c) 2013 Kristian Wiklund
#All rights reserved.

#Redistribution and use in source and binary forms are permitted
#provided that the above copyright notice and this paragraph are
#duplicated in all such forms and that any documentation,
#advertising materials, and other materials related to such
#distribution and use acknowledge that the software was developed
#by the <organization>.  The name of the
#<organization> may not be used to endorse or promote products derived
#from this software without specific prior written permission.
#THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
#IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

# this is a wickedly clever script that traverse all disks and look for their contents...
# this will take a long time. 

# basically this is how it works. It searches for d64 disks in the parent directory and
# inspects the contents for potential executables
# a file execs.sql is created, which can be used to populate a sqlite3 database

# heuristics: look for files being loaded at 0x801, containing at least one BASIC "SYS" command
# yes, we are missing files but it is the only practical way to do it

# tapes should be included too, not done
# this is not tested on RPI, only on Solaris

rm -f execs.sql
echo "begin transaction;" > execs.sql

IFS=$'\n'

for i in `find .. -name "*.d64"`; do
    echo $i
    # invoke c1541 utility to check the contents

    
    for j in `c1541 $i -list | grep "prg"| awk -F\" '{print $2}'`; do
	#echo $j
	# invoke c1541 utility to extract the prg files to /tmp, and check the loading adress
	
	c1541 "$i" -read "$j" /tmp/apa.bin
	la=`xxd -l2 /tmp/apa.bin | cut -d' ' -f2`
	
	if [ "x$la" = x0108 ]; then #match
	    
	    ls=`petcat /tmp/apa.bin|grep -i sys`
	    
	    if [ "x$ls" != "x" ]; then #match
#		echo "\"$i\":\"$j\"" >> executables.txt
#		p=`php -e "echo sqlite_escape_string($argv[1])" $j`
		k=`echo $j|sed "su\\\'u\\\\\'ug"`
		echo "insert into prgs (disk,name,broken,favorite,killfile) values ('$i','$k',0,0,0);">> execs.sql
	    fi
	fi

    done

done

echo "commit transaction;" >> execs.sql
