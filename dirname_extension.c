/* Copyright (c) 2013 Kristian Wiklund
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the <organization>.  The name of the
 * <organization> may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

#define NULL 0

#include <sqlite3ext.h> /* Do not use <sqlite3.h>! */
SQLITE_EXTENSION_INIT1

/* Insert your extension code here */

#ifdef _WIN32
__declspec(dllexport)
#endif

#include <libgen.h>

void mydirname(sqlite3_context* context,int argc,sqlite3_value** argv) {

  char *text;

  if (argc == 1) {
    text = sqlite3_value_text(argv[0]);
    if (text) {
      char *result;
      if(!(result = dirname(text)))
	  sqlite3_result_null(context);

      //      printf("%s %s\n", text, dirname(text));
      sqlite3_result_text(context, result, -1, SQLITE_TRANSIENT);
      return;
    }
  }
  sqlite3_result_null(context);

}

/* TODO: Change the entry point name so that "extension" is replaced by
** text derived from the shared library filename as follows:  Copy every
** ASCII alphabetic character from the filename after the last "/" through
** the next following ".", converting each character to lowercase, and
** discarding the first three characters if they are "lib".
*/
int sqlite3_dirnameextension_init(
			   sqlite3 *db, 
			   char **pzErrMsg, 
  const sqlite3_api_routines *pApi
			   ){
  int rc = SQLITE_OK;
  SQLITE_EXTENSION_INIT2(pApi);

  rc = sqlite3_create_function_v2(db,
			     "dirname",
			     1,
			     SQLITE_ANY,
			     0,
			     mydirname,
			     NULL,
			     NULL,
			     NULL

			     );
  /* Insert here calls to
  **     sqlite3_create_function_v2(),
  **     sqlite3_create_collation_v2(),
  **     sqlite3_create_module_v2(), and/or
  **     sqlite3_vfs_register()
  ** to register the new features that your extension adds.
  */
  return rc;
}
