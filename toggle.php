<?php

/* Copyright (c) 2013 Kristian Wiklund
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the <organization>.  The name of the
 * <organization> may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

// update "toggle" values in database

// arguments:
// table - what table the toggle is for, e.g. "prgs"
// key - what key it is
// value - the value

// very simple, completely insane design as it can update and change whatever in the database ;)

require_once("config.php");

$table = $_REQUEST["table"];
$key = $_REQUEST["key"];
$rowid = $_REQUEST["rowid"];
$value = ($_REQUEST["value"]=="true"?1:0);

$db->query("update $table set $key=$value where rowid=$rowid;");

?>