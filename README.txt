Disclaimer: This software is not suitable for anything, so you are on your own. Read LICENSE.txt
Disclaimer: The code is ugly but it works. Feel free to refactor.

****

Important: This may require that VICE is built from the latest CVN with the "no crash" patch applied.
Important: Patch -> http://sourceforge.net/p/vice-emu/patches/84/
Important: VICE SVN: http://sourceforge.net/p/vice-emu/code/HEAD/tree/

***


This is a small, quickly hacked together, web "app" to control
the "vice/x64" commodore c64 emulator from a smartphone. Instead
of using the emulator interface to select floppies and load 
software, you use the phone to navigate your library.

Why do you need that?

Assume that you are building for example a "Pi C64", 
http://hackaday.com/2012/07/09/refurbing-a-c64-with-a-raspberry-pi/,
that is, a C64 case with a small computer executing the emulator. 

For the most immersive retro experience, it would be nice to avoid
flipping disks with the interface in the emulator.

****

Things implemented:

- Navigate the database, through a sorted list with software, and click-to-start
- The code to add/remove favorites, ratings, etc

Planned things not implemented yet:

- Search
- Disk browser (in addition to title browser)
- Reseting sound (if sound is broken, it gets fixed by briefly changing drivers)
(C:$f8d3) resget "SoundDeviceName"
SoundDeviceName="sdl"
(C:$f8d3) resset "SoundDeviceName" "alsa"
(C:$f8d3) resget "SoundDeviceName"
SoundDeviceName="alsa"
(C:$f8d3)

Not yet investigated things not implemented yet:
- Using something like gamebase to get info about the games

****

How it works:

- IUI (http://www.iui-js.org/) is used to provide the user interface
- A sqlite3 database is the data storage
- The database is populated using (very simple) heuristics in a shell script
- PHP logic is used to generate the web pages

****

Dependencies:

- php
- php sqlite3
- sqlite3

I use it with lighttpd on a raspberry pi, the same that is running the emulator. 

****

Installation:

Unpack/clone to web root or wherever you want to access the "app"
Create a "database" directory within the directory containing your .d64 files
Change config.php to your paths
Go to the database directory. Execute the populatedb.sh script. This will take a long time.
Run "sqlite3 files.db < execs.sql" 
Done. You can use your web browser to control the emulator, if all was configured okay

