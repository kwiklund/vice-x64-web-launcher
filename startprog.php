<?php

/* Copyright (c) 2013 Kristian Wiklund
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the <organization>.  The name of the
 * <organization> may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

require_once("config.php");
require_once("sockets.php");

$rowid = $_REQUEST["y"];	

$q = $db->query("select * from prgs where rowid=$rowid");
$res = $q->fetchArray(SQLITE3_ASSOC);

// CREATE TABLE prgs (disk text, name text, broken integer, favorite integer, killfile integer);

$title = $res["name"]; 
$disk = $res["disk"];

echo "<form id='gamestarter' class='panel' title='$title'>";

echo "<h1>Loading...</H1>";

	c64connectsocket();

	   c64command("cd $dbloc");
	   c64command("reset 1");
	   c64command("attach \"$disk\" 8");
	   c64command("autostart \"$disk:$title\" 8");

	   c64closesocket();

?>
