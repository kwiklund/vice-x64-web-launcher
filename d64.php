<?php

/* Copyright (c) 2013 Kristian Wiklund
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the <organization>.  The name of the
 * <organization> may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

// methods to provide access to .d64 files

// tech ref: http://unusedino.de/ec64/technical/formats/d64.html

// * "The standard D64 is a 174848 byte file comprised of 256 byte
// sectors arranged in 35 tracks with a varying number of  sectors  per  track
// for a total of 683 sectors. Track counting starts at 1, not 0, and goes  up
// to 35. Sector counting starts at 0, not 1, for the first sector,  therefore
// a track with 21 sectors will go from 0 to 20"

// * "The directory track should be contained totally on track 18. Sectors 1-18
// contain the entries and sector 0 contains the BAM (Block Availability  Map)
// and disk name/ID. Since the directory is only 18 sectors large (19 less one
// for the BAM), and each sector can contain only  8  entries  (32  bytes  per
// entry), the maximum number of directory entries is 18 * 8 = 144. The  first
// directory sector is always 18/1, even though the t/s pointer at 18/0 (first
// two bytes) might point somewhere else.  It  then  follows  the  same  chain
// structure as a normal file, using a sector interleave of 3. This makes  the
// chain links go 18/1, 18/4, 18/7 etc."

 // Track #Sect #SectorsIn D64 Offset   Track #Sect #SectorsIn D64 Offset
 //  ----- ----- ---------- ----------   ----- ----- ---------- ----------
 //    1     21       0       $00000      21     19     414       $19E00
 //    2     21      21       $01500      22     19     433       $1B100
 //    3     21      42       $02A00      23     19     452       $1C400
 //    4     21      63       $03F00      24     19     471       $1D700
 //    5     21      84       $05400      25     18     490       $1EA00
 //    6     21     105       $06900      26     18     508       $1FC00
 //    7     21     126       $07E00      27     18     526       $20E00
 //    8     21     147       $09300      28     18     544       $22000
 //    9     21     168       $0A800      29     18     562       $23200
 //   10     21     189       $0BD00      30     18     580       $24400
 //   11     21     210       $0D200      31     17     598       $25600
 //   12     21     231       $0E700      32     17     615       $26700
 //   13     21     252       $0FC00      33     17     632       $27800
 //   14     21     273       $11100      34     17     649       $28900
 //   15     21     294       $12600      35     17     666       $29A00
 //   16     21     315       $13B00      36(*)  17     683       $2AB00
 //   17     21     336       $15000      37(*)  17     700       $2BC00
 //   18     19     357       $16500      38(*)  17     717       $2CD00
 //   19     19     376       $17800      39(*)  17     734       $2DE00
 //   20     19     395       $18B00      40(*)  17     751       $2EF00  

// this means that the directory starts at track 18 which according to the table is at
// byte offset $16500. The BAM is the first sector, and so, the directory entries start
// at $16600

class d64 {


  // constructor. nothing strange, give it a path to the disk, open it.
  function __construct($path) {
    $this->path = $path;



    if(!($this->d64 = fopen($path, "rb"))) {
      throw new NotFoundException();
      }

  $this->tracks = array(-1, 0, 0x1500, 0x2A00, 0x3F00, 0x5400, 0x6900, 0x7E00, 
		  0x9300, 0xA800, 0xBD00, 0xD200, 0xE700, 0xFC00, 0x11100,
		  0x12600, 0x13B00, 0x15000, 0x16500, 0x17800, 0x18B00,
		  0x19E00, 0x1B100, 0x1C400, 0x20E00, 0x22000, 0x23200,
		  0x24400, 0x25600, 0x26700, 0x27800, 0x28900, 0x29A00,
		  0x2AB00, 0x2BC00, 0x2CD00, 0x2DE00, 0x2EF00);
		      
  $this->maxtrack=40; // fix later if necessary, to fit d64 image loaded

  $this->opendirptr = 0; // an array(track, sector, offset)

  }

  function maxsector($track) {

    if($track<0 || $track>$this->maxtrack)    return FALSE;

    if($track < 18) return 20;
    if($track < 25) return 18;
    if($track < 31) return 17;
    
    return 16;

    }

    // pretty print type name
//Bit 0-3: The actual filetype
  //                        000 (0) - DEL
    //                      001 (1) - SEQ
      //                    010 (2) - PRG
        //                  011 (3) - USR
          //                100 (4) - REL 
   function typename($type) {
   	    switch($type & 0x7) {
	    		 case 0: return "DEL";
			 case 1: return "SEQ";
			 case 2: return "PRG";
			 case 3: return "USR";
			 case 4: return "REL";
			 }
	    return FALSE;
}

  // readsector
  function readsector($track, $sector) {

    // sanity checks
    if($track<1 || $track>$this->maxtrack) return FALSE;
    if(!($ms = $this->maxsector($track))) return FALSE;
    if($sector>$ms) return FALSE;
    
    // seek, readm and return the sector as an array

    if(-1 == fseek($this->d64, $this->tracks[$track]+256*$sector)) return FALSE;

    if(!($data = fread($this->d64,256))) return FALSE;

    return str_split($data);
    }

  // read the next sector, compared to the one in the data 

  function readnextsector($data) {
    return $this->readsector($data[0],$data[1]);
    }

  // opendir - initialize the dir pointer

  function opendir() {
    $this->opendirptr = array("track"=>18,"sector"=>1,"offset"=>0); // byte 0-1 is the next sector ptr
  }

  // readdir. return an array(filename,type) on success, FALSE on fail or end of dir
  // doing a lot of rading here, but I'm going for functionality and not efficiency now.
  // a better way would be to chache the reads somehow
  function readdir() {
    if($this->opendirptr == 0) return FALSE;
    
    $data = $this->readsector($this->opendirptr["track"], $this->opendirptr["sector"]);


//    echo bin2hex(implode("",array_slice($data,$this->opendirptr["offset"],0x20)));
    // read one entry, then step the pointer
    // byte 2 in the struct is the type
    $type = ord($data[$this->opendirptr["offset"]+2]);
    if($type!=0) {

      // then we are interested in the filename. 

      $name = implode("",array_slice($data,$this->opendirptr["offset"]+5,16));
    
      // file name is padded with A0 of all things. Cut at the first A0.
      $pos = strpos($name, 0xA0);
      $name = substr($name,0,$pos);
     }
      if(($this->opendirptr["offset"] += 0x20) > 0xFF) { // increase pointer, get next sector if needed
							//      echo "\n>".bin2hex(implode("",array_slice($data,0,0x20)))."<\n";   
 
      $x=ord($data[0]);
      $y=ord($data[1]);
						      
      $this->opendirptr["track"] = $x;	
      $this->opendirptr["sector"] = $y;	
      $this->opendirptr["offset"] = 0;	

    // check if we are at end of dir or not
    if($x==0 && $y==0xFF) {
	$this->opendirptr = 0;
	}
    
//     printf("setting the next sector to (%d,%d)\n", $x,$y);		
    }
    if($type==0)
      return $this->readdir();
    else
      return array($name, $this->typename($type));
  }

}

// test.d64 contains the following:
// 0 "test            " 00 2a
// and then 9 entries 1-9 (we need 9 to cross sector boundary)
// file 1 is deleted (scratched, type=0)

if(php_sapi_name() == "cli") { // execute tests if invoked from cli
  
  if($disk = new d64("test.d64")) {
    echo("successfully opened test diskette\n");
  }			 
 $disk->opendir();			      

 while($x = $disk->readdir()) {
 	  printf("entry: %16s : %s\n", $x[0], $x[1]);
 }


}

