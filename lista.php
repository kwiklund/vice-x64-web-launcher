<?php

/* Copyright (c) 2013 Kristian Wiklund
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the <organization>.  The name of the
 * <organization> may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */
require_once("config.php");

$x = $_REQUEST["x"];
echo "<ul id='$x' title='$x'>";
echo "<li class='group'>$x</li>";	

// here we make a list of all execs starting with $x

// CREATE TABLE prgs (disk text, name text, broken integer, favorite integer, killfile integer);



if($x == "[0-9]")
$expr = "name glob '[0-9]*'";
else
if($x == "<space>")
$expr = "name like ' %'";
else
if($x == "fav")
$expr = "favorite=1";
else 
$expr = "name like '$x%'";

//	echo "select rowid,name from prgs where $expr and broken=0 and killfile=0 order by name asc;";	
$q = $db->query("select rowid,name from prgs where $expr and broken=0 and killfile=0 order by name collate nocase asc;");

while($res = $q->fetchArray(SQLITE3_ASSOC)) {
  $rowid=$res["rowid"];
  $name=$res["name"];
  echo "<li><a href='showprog.php?y=$rowid'>".htmlentities($name)."</a></li>";	
}
echo "</ul>";

?>
