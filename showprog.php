<?php

/* Copyright (c) 2013 Kristian Wiklund
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that the above copyright notice and this paragraph are
 * duplicated in all such forms and that any documentation,
 * advertising materials, and other materials related to such
 * distribution and use acknowledge that the software was developed
 * by the <organization>.  The name of the
 * <organization> may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */
require_once("config.php");

$rowid = $_REQUEST["y"];	

$q = $db->query("select * from prgs where rowid=$rowid");
$res = $q->fetchArray(SQLITE3_ASSOC);

// CREATE TABLE prgs (disk text, name text, broken integer, favorite integer, killfile integer);

$title = $res["name"]; 
$broken = $res["broken"];
$boring = $res["killfile"];
$favorite = $res["favorite"];
?>	

<form id='program' class='panel' title='<?php echo $title;?>' action='startprog.php'>
  
  <input type="hidden" name="y" value="<?php echo $rowid;?>">

  <fieldset>

    <div class="row">
      <label>Broken</label>
      <div id="broken" class="toggle" onclick="javascript:toggleSub(<?php echo $rowid;?>,'prgs','broken');" <?php if($broken==1) echo 'toggled="true"';?>><span class="thumb"></span><span class="toggleOn">ON</span><span class="toggleOff">OFF</span></div>
    </div>

    <div class="row">
      <label>Boring</label>
      <div id="killfile" class="toggle" onclick="javascript:toggleSub(<?php echo $rowid;?>,'prgs','killfile');" <?php if($boring==1) echo 'toggled="true"';?>><span class="thumb"></span><span class="toggleOn">ON</span><span class="toggleOff">OFF</span></div>
</div>

    <div class="row">
      <label>Favorite</label>
      <div id="favorite" class="toggle" onclick="javascript:toggleSub(<?php echo $rowid;?>,'prgs','favorite');" <?php if($favorite==1) echo 'toggled="true"';?>><span class="thumb"></span><span class="toggleOn">ON</span><span class="toggleOff">OFF</span></div>
</div>

  </fieldset>
  <a class="whiteButton" type="submit" href="javascript:formname.submit()">Start Program</a>

</form>;

