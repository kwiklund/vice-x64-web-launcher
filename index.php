<!DOCTYPE html>
<!--
  -- Copyright (c) 2013 Kristian Wiklund
  -- All rights reserved.
  -- 
  -- Redistribution and use in source and binary forms are permitted
  -- provided that the above copyright notice and this paragraph are
  -- duplicated in all such forms and that any documentation,
  -- advertising materials, and other materials related to such
  -- distribution and use acknowledge that the software was developed
  -- by the <organization>.  The name of the
  -- <organization> may not be used to endorse or promote products derived
  -- from this software without specific prior written permission.
  -- THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
  -- IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
  -- WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
  -->

<head>
  <title>C64 Control Panel</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <link rel="stylesheet" href="iui/iui.css" type="text/css" />
  <link rel="stylesheet" href="iui/ext-sandbox/ext-css/iui-lists.css" type="text/css" />
  <link rel="stylesheet" id="iui-theme" title="iUI Theme" href="iui/t/default/default-theme.css"  type="text/css"/>
  <script type="application/x-javascript" src="iui/iui.js"></script>
  <script type="application/x-javascript" src="iui/js/iui-theme-switcher.js"></script>
  <script type="application/x-javascript">iui.ts.basePath = "iui/";</script>
  <script type="application/x-javascript" src="iui/ext-sandbox/cache-manager/iui-cache-manager.js"></script>

  <script type="application/x-javascript" src="fulkod.js"></script>

</head>

<body>
  <div class="toolbar">
    <h1 id="pageTitle"></h1>
    <a id="backButton" class="button" href="#"></a>
    <a class="button" href="actions.php?command=reset">Reset</a>
    <!--        <a class="button" href="#searchForm">Search</a>-->
  </div>

  <ul id="home" title="C64 Control Center" selected="true">
      <li><a href='lista.php?x=fav'>Favorites</a></li>
    <li><a href="#apps">Applications</a></li>
    <li><a href="#disks">Diskettes</a></li>
  </UL>


  <ul id="apps" title="Applications">    
    
    <?php

       require_once("config.php");

       echo "<li class='group'>Shortcuts</li>";	
       echo "<li><a href='lista.php?x=fav'>Favorites</a></li>";
       //	 echo "<li><a href='lista.php?x=fav'>Recently Used</a></li>";
       echo "<li class='group'>Program Names</li>";	
       $q=$db->query("SELECT count(*) as c,substr(name,1,1) AS l FROM prgs group by l order by l asc");

    while($res = $q->fetchArray(SQLITE3_ASSOC)) {
   //   $cnt = $db->querySingle("select count(*) from prgs where name like '$v%';");
      $l=$res["l"];

      if($l==" ")
        $l="&lt;space&gt;";
      else
        $l=htmlentities($l);

      echo "<li><a href='lista.php?x=".$l."'>".$l."<span class='bubble'>".$res["c"]."</span></a></li>";
    }

       ?>
  </ul>

  <!--
      <form id="searchForm" class="dialog" action="search.gtpl" method="GET">
        <fieldset>
          <h1>Music Search</h1>
          <a class="button leftButton" type="cancel">Cancel</a>
          <a class="button blueButton" type="submit">Search</a>
          
          <label>Artist:</label>
          <input id="artist" type="text" name="artist"/>
          <label>Song:</label>
          <input type="text" name="song"/>
        </fieldset>
      </form>
      -->

</body>
</html>
